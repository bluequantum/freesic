import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:freesic/component.dart';
import 'package:freesic/component/global/orientation.dart';
import 'package:freesic/pages/account/page_user_detail.dart';
import 'package:freesic/pages/main/main_page_discover.dart';
// import 'package:freesic/pages/search/page_search.dart';
import 'package:freesic/part/part.dart';
import 'package:url_launcher/url_launcher.dart';

import 'drawer.dart';
import 'my/main_page_my.dart';

part 'page_main_landscape.dart';
part 'page_main_portrait.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return context.isLandscape ? _LandscapeMainPage() : _PortraitMainPage();
  }
}

extension LandscapeMainContext on BuildContext {
  /// Obtain the primary navigator for landscape mode.
  NavigatorState? get landscapePrimaryNavigator =>
      findAncestorStateOfType<_LandscapeMainPageState>()!
          ._landscapeNavigatorKey
          .currentState;

  /// Obtain the secondary navigator for landscape mode.
  NavigatorState? get landscapeSecondaryNavigator =>
      findAncestorStateOfType<_LandscapeMainPageState>()!
          ._landscapeSecondaryNavigatorKey
          .currentState;
}
