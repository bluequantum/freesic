// import 'dart:ui';

// import 'package:flutter/material.dart';
// import 'package:freesic/component.dart';
// import 'package:netease_music_api/netease_music_api.dart';

// import '/pages/playlist/music_list.dart';
// import '/pages/playlist/page_playlist_detail.dart';
// import '/part/part.dart';
// import '/repository/netease.dart';

// class MainPageDiscover extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() => CloudPageState();
// }

// class CloudPageState extends State<MainPageDiscover>
//     with AutomaticKeepAliveClientMixin {
//   @override
//   bool get wantKeepAlive => true;

//   @override
//   Widget build(BuildContext context) {
//     super.build(context);
//     return ListView(
//       children: <Widget>[
//         _NavigationLine(),
//         _Header("推荐歌单", () {}),
//         _SectionPlaylist(),
//         _Header("最新音乐", () {}),
//         _SectionNewSongs(),
//       ],
//     );
//   }
// }

// class _NavigationLine extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       margin: const EdgeInsets.symmetric(vertical: 16),
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//         children: <Widget>[
//           _ItemNavigator(Icons.today, "每日推荐",
//               () => context.secondaryNavigator!.pushNamed(pageDaily)),
//           _ItemNavigator(Icons.show_chart, "排行榜",
//               () => context.secondaryNavigator!.pushNamed(pageLeaderboard)),
//         ],
//       ),
//     );
//   }
// }

// ///common header for section
// class _Header extends StatelessWidget {
//   const _Header(this.text, this.onTap);

//   final String text;
//   final GestureTapCallback onTap;

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       margin: const EdgeInsets.only(top: 10, bottom: 6),
//       child: Row(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: <Widget>[
//           const Padding(padding: EdgeInsets.only(left: 8)),
//           Text(
//             text,
//             style: Theme.of(context)
//                 .textTheme
//                 .subtitle1!
//                 .copyWith(fontWeight: FontWeight.w800),
//           ),
//           const Icon(Icons.chevron_right),
//         ],
//       ),
//     );
//   }
// }

// class _ItemNavigator extends StatelessWidget {
//   const _ItemNavigator(this.icon, this.text, this.onTap);

//   final IconData icon;

//   final String text;

//   final GestureTapCallback onTap;

//   @override
//   Widget build(BuildContext context) {
//     return InkWell(
//         onTap: onTap,
//         child: Container(
//           padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
//           child: Column(
//             children: <Widget>[
//               Material(
//                 shape: const CircleBorder(),
//                 elevation: 5,
//                 child: ClipOval(
//                   child: Container(
//                     width: 40,
//                     height: 40,
//                     color: Theme.of(context).primaryColor,
//                     child: Icon(
//                       icon,
//                       color: Theme.of(context).primaryIconTheme.color,
//                     ),
//                   ),
//                 ),
//               ),
//               Padding(padding: const EdgeInsets.only(top: 8)),
//               Text(text),
//             ],
//           ),
//         ));
//   }
// }

// class _SectionPlaylist extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return FutureBuilder(
//       future: NeteaseMusicApi().personalizedPlaylist(limit: 6),
//       builder: (context, AsyncSnapshot<PersonalizedPlayListWrap> result) {
//         if (result.connectionState == ConnectionState.done) {
//           return LayoutBuilder(builder: (context, constraints) {
//             assert(constraints.maxWidth.isFinite,
//                 "can not layout playlist item in infinite width container.");
//             final parentWidth = constraints.maxWidth - 8;
//             const int count = /* false ? 6 : */ 3;
//             final double width =
//                 (parentWidth ~/ count).toDouble().clamp(80.0, 200.0);
//             final double spacing = (parentWidth - width * count) / (count + 1);
//             return Padding(
//               padding:
//                   EdgeInsets.symmetric(horizontal: 4 + spacing.roundToDouble()),
//               child: Wrap(
//                 spacing: spacing,
//                 children: result.requireData.result!
//                     .map((e) => _PlayListItemView(
//                           playlist: e,
//                           width: width,
//                         ))
//                     .toList(),
//               ),
//             );
//           });
//         } else {
//           return Container();
//         }
//       },
//     );
//   }
// }

// class _PlayListItemView extends StatelessWidget {
//   const _PlayListItemView({
//     Key? key,
//     required this.playlist,
//     required this.width,
//   }) : super(key: key);

//   final Play playlist;

//   final double width;

//   @override
//   Widget build(BuildContext context) {
//     GestureLongPressCallback? onLongPress;

//     String? copyWrite = playlist.copywriter;
//     if (copyWrite != null && copyWrite.isNotEmpty) {
//       onLongPress = () {
//         showDialog(
//             context: context,
//             builder: (context) {
//               return AlertDialog(
//                 content: Text(
//                   playlist.copywriter!,
//                   style: Theme.of(context).textTheme.bodyText1,
//                 ),
//               );
//             });
//       };
//     }

//     return InkWell(
//       onTap: () {
//         context.secondaryNavigator!.push(MaterialPageRoute(builder: (context) {
//           return PlaylistDetailPage(
//           playlist.id,
//           );
//           return Container();
//         }));
//       },
//       onLongPress: onLongPress,
//       child: Container(
//         child: Container(
//           width: width,
//           padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 8),
//           child: Column(
//             children: <Widget>[
//               Container(
//                 height: width,
//                 width: width,
//                 child: ClipRRect(
//                   borderRadius: BorderRadius.all(Radius.circular(6)),
//                   child: AspectRatio(
//                     aspectRatio: 1,
//                     child: FadeInImage(
//                       placeholder:
//                           const AssetImage("assets/playlist_playlist.9.png"),
//                       image: CachedImage(playlist.coverImgUrl ??
//                           "https://bluequantum.gitee.io/favicon.ico"),
//                       fit: BoxFit.cover,
//                     ),
//                   ),
//                 ),
//               ),
//               const Padding(padding: EdgeInsets.only(top: 4)),
//               Text(
//                 playlist.name ?? "",
//                 maxLines: 2,
//                 overflow: TextOverflow.ellipsis,
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

// class _SectionNewSongs extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return FutureBuilder(
//       future: NeteaseMusicApi().personalizedSongList(),
//       builder: (context, AsyncSnapshot<PersonalizedSongListWrap> result) {
//         if (result.connectionState == ConnectionState.done) {
//           final songs = result.data!.result!
//               .map((e) => MediaDetails.fromSong(e.song))
//               .toList();
//           // return MusicTileConfiguration(
//           //   musics: songs,
//           //   token: 'playlist_main_newsong',
//           //   onMusicTap: MusicTileConfiguration.defaultOnTap,
//           //   leadingBuilder: MusicTileConfiguration.indexedLeadingBuilder,
//           //   trailingBuilder: MusicTileConfiguration.defaultTrailingBuilder,
//           //   child: Column(
//           //     children: songs.map((m) => MusicTile(m)).toList(),
//           //   ),
//           // );
//           return Container();
//         } else {
//           return Container();
//         }
//       },
//     );
//   }
// }
