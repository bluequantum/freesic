import 'package:freesic/part/part.dart';
import 'package:just_audio_background/just_audio_background.dart';
import 'package:netease_music_api/netease_music_api.dart' hide Music;

class MediaDetails extends MediaItem {
  MediaDetails(Song2 song)
      : artists = song.ar ?? [],
        super(
          id: song.id,
          title: song.name!,
          artUri: Uri.parse(song.al.picUrl!.replaceFirst("http:", "https:")),
        );
  MediaDetails.fromSong(Song song)
      : artists = song.artists ?? [],
        super(
          id: song.id,
          title: song.name!,
          artUri:
              Uri.parse(song.album!.picUrl!.replaceFirst("http:", "https:")),
        );

  List<Artists> artists;

  @override
  String get artist => artists.toString();

  Music toMusic() {
    return Music(id: int.parse(id), title: title, album: album);
  }
}
