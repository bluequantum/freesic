import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:netease_music_api/netease_music_api.dart';

import 'database.dart';

LocalData neteaseLocalData = LocalData._();

extension _PlayToMap on Play {
  Map toMap() {
    return Map();
  }
}

extension _MapToPlay on Map {
  Play toPlay() {
    return Play();
  }
}

class LocalData {
  ///netData 类型必须是可以放入 [store] 中的类型
  static Stream<T> withData<T>(String key, Future<T> netData,
      {void onNetError(dynamic e)?}) async* {
    final data = neteaseLocalData[key];
    if (data != null) {
      final cached = await data;
      if (cached != null) {
        assert(cached is T, "local espect be $T, but is $cached");
        yield cached as T;
      }
    }
    try {
      final net = await netData;
      neteaseLocalData[key] = net;
      yield net;
    } catch (e) {
      if (onNetError != null) onNetError("$e");
      debugPrint('error : $e');
    }
  }

  LocalData._();

  FutureOr operator [](key) async {
    return get(key);
  }

  void operator []=(key, value) {
    _put(value, key);
  }

  Future<T?> get<T>(dynamic key) async {
    final Database db = await getApplicationDatabase();
    final dynamic result = await StoreRef.main().record(key).get(db);
    if (result is T) {
      return result;
    }
    assert(false, "the result of $key is not subtype of $T. $result");
    return null;
  }

  Future _put(dynamic value, [dynamic key]) async {
    final Database db = await getApplicationDatabase();
    return StoreRef.main().record(key).put(db, value);
  }

  Future<List<Play?>?> getUserPlaylist(int? userId) async {
    final data = await get("user_playlist_$userId");
    if (data == null) {
      return null;
    }
    final result = (data as List).cast<Map>().map((m) => m.toPlay()).toList();
    return result;
  }

  void updateUserPlaylist(int? userId, List<Play?> list) {
    _put(list.map((p) => p!.toMap()).toList(), "user_playlist_$userId");
  }

  Future<Play?> getPlaylistDetail(int playlistId) async {
    final data = await get("playlist_detail_$playlistId");
    return (data as Map).toPlay();
  }

  //TODO 添加分页加载逻辑
  Future updatePlaylistDetail(Play playlistDetail) {
    return _put(playlistDetail.toMap(), 'playlist_detail_${playlistDetail.id}');
  }
}
