import 'package:json_annotation/json_annotation.dart';
import 'package:just_audio_background/just_audio_background.dart';

import 'model.dart';

part 'music.g.dart';

@JsonSerializable()
class Music implements MediaItem {
  Music({
    required int id,
    required this.title,
    this.album,
    // this.artist,
  }) : _id = id;

  factory Music.fromJson(Map<String, dynamic> json) {
    return _$MusicFromJson(json);
  }

  final int _id;

  @override
  String get id => _id.toString();

  final String title;
  // @override
  // String get title => _title;

  final String? album;

  // final List<Artist>? artist;

  String? get imageUrl => artUri.toString();

  String get artistString => artist ?? "";

  String get subTitle {
    final ar = artist;
    final al = album;
    if (ar == null && al == null) {
      return '';
    }
    if (ar == null) {
      return al!;
    } else if (al == null) {
      return ar;
    } else {
      return "$al - $ar";
    }
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is Music && id == other.id;

  @override
  int get hashCode => id.hashCode;

  Map<String, dynamic> toJson() => _$MusicToJson(this);

  @override
  noSuchMethod(Invocation invocation) => super.noSuchMethod(invocation);
}
