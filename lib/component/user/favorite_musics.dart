// import 'dart:async';

// import 'package:flutter_riverpod/flutter_riverpod.dart';
// import 'package:freesic/component.dart';
// import 'package:freesic/model/model.dart';
// import 'package:freesic/part/part.dart';
// import 'package:freesic/repository/netease.dart';
// import 'package:netease_music_api/netease_music_api.dart';

// final userFavoriteMusicListProvider =
//     StateNotifierProvider<UserFavoriteMusicListNotifier, List<int>>(
//   (ref) => UserFavoriteMusicListNotifier(ref.watch(userProvider).userId),
// );

// class UserFavoriteMusicListNotifier extends CacheableStateNotifier<List<int>> {
//   UserFavoriteMusicListNotifier(this.userId) : super(const []);

//   static const _keyLikedSongList = 'likedSongList';

//   final int? userId;

//   /// 红心歌曲
//   Future<void> likeMusic(MediaDetails music) async {
//     final succeed = await NeteaseMusicApi().likeSong(music.id.toString(), true);
//     if (succeed.code == RET_CODE_OK) {
//       state = [...state, int.parse(music.id)];
//     }
//   }

//   ///取消红心歌曲
//   Future<void> dislikeMusic(MediaDetails music) async {
//     final succeed = await NeteaseMusicApi().likeSong(music.id.toString(), false);
//     if (succeed.code == RET_CODE_OK) {
//       state = List.from(state)..remove(music.id);
//     }
//   }

//   @override
//   Future<List<int>?> load() async {
//     final value = await NeteaseMusicApi().likeSongList(userId.toString());
//     if (value.codeEnum == ) {
//       return value.asValue!.value;
//     }
//     return null;
//   }

//   @override
//   Future<List<int>?> loadFromCache() async =>
//       (await neteaseLocalData[_keyLikedSongList] as List?)?.cast<int>();

//   @override
//   void saveToCache(List<int> value) {
//     neteaseLocalData[_keyLikedSongList] = value;
//   }
// }

// final musicIsFavoriteProvider = Provider.family<bool, Music>((ref, music) {
//   return ref.watch(userFavoriteMusicListProvider).contains(music.id);
// });
