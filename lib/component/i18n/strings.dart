import 'package:flutter/widgets.dart';
import 'package:freesic/generated/l10n.dart';

export 'package:freesic/generated/l10n.dart';

extension AppStringResourceExtension on BuildContext {
  S get strings {
    return S.of(this);
  }
}
