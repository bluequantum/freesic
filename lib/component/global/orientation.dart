import 'package:flutter/material.dart';
import 'package:freesic/pages/main/page_main.dart';

extension OrientationContext on BuildContext {
  NavigatorState get rootNavigator => Navigator.of(this, rootNavigator: true);

  NavigatorState? get primaryNavigator => isLandscape ? landscapePrimaryNavigator : Navigator.of(this);

  NavigatorState? get secondaryNavigator => isLandscape ? landscapeSecondaryNavigator : Navigator.of(this);

  /// check current application orientation is landscape.
  bool get isLandscape => MediaQuery.of(this).orientation == Orientation.landscape;

  bool get isPortrait => !isLandscape;
}
