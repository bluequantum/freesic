import 'package:flutter/material.dart';

// import '/pages/collection/page_collections.dart';
import '/pages/leaderboard/page_leaderboard.dart';
import '/pages/main/page_main.dart';
import '/pages/player/page_playing.dart';
// import '/pages/playlist/page_daily_playlist.dart';
import '/pages/setting/page_setting.dart';
import '/pages/welcome/login_sub_navigation.dart';
import '/pages/welcome/page_welcome.dart';

// export '/pages/collection/page_collections.dart';
export '/pages/leaderboard/page_leaderboard.dart';
export '/pages/main/page_main.dart';
export '/pages/player/page_playing.dart';
// export '/pages/playlist/page_album_detail.dart';
// export '/pages/playlist/page_daily_playlist.dart';
// export '/pages/playlist/page_playlist_detail.dart';
export '/pages/setting/page_setting.dart';

const pageMain = Navigator.defaultRouteName;

///popup with [true] if login succeed
const pageLogin = "/login";

const ROUTE_PLAYLIST_DETAIL = "/playlist/detail";

/// Route name of [PlayingPage].
const pagePlaying = "/playing";

/// 私人FM
const pageFmPlaying = "/playing_fm";

const pageLeaderboard = "/leaderboard";

/// Route name of [DailyPlaylistPage]
const pageDaily = "/daily";

const pageMyDj = '/mydj';

const ROUTE_MY_COLLECTION = '/my_collection';

const ROUTE_SETTING = '/setting';

const ROUTE_SETTING_THEME = '/setting/theme';

const pageWelcome = 'welcome';

/// Search page route name
const pageSearch = "search";

///app routers
final Map<String, WidgetBuilder> routes = {
  pageMain: (context) => MainPage(),
  pageLogin: (context) => LoginNavigator(),
  pagePlaying: (context) => PlayingPage(),
  // pageLeaderboard: (context) => LeaderboardPage(),
  // pageDaily: (context) => DailyPlaylistPage(),
  // ROUTE_MY_COLLECTION: (context) => MyCollectionPage(),
  ROUTE_SETTING: (context) => SettingPage(),
  ROUTE_SETTING_THEME: (context) => SettingThemePage(),
  pageWelcome: (context) => PageWelcome(),
};

Route<dynamic>? routeFactory(RouteSettings settings) {
  WidgetBuilder? builder;

  if (builder != null) {
    return MaterialPageRoute(builder: builder, settings: settings);
  }

  assert(false, 'ERROR: can not generate Route for ${settings.name}');
  return null;
}
