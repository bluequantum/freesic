part of 'player.dart';

/// Music metadata
class MusicMetadata {
  final String mediaId;
  final String? mediaUri;
  final String? title;
  final String? subtitle;

  final int duration;
  final String? iconUri;

  MusicMetadata({
    required this.mediaId,
    this.title,
    this.subtitle,
    this.duration = 0,
    this.iconUri,
    this.mediaUri,
  });

  factory MusicMetadata.fromMap(Map map) {
    return MusicMetadata(
      mediaId: map["mediaId"],
      title: map["title"],
      subtitle: map["subtitle"],
      duration: map["duration"],
      iconUri: map["iconUri"],
      mediaUri: map['mediaUri'],
    );
  }

  Map toMap() {
    return {
      "mediaId": mediaId,
      "title": title,
      "subtitle": subtitle,
      "duration": duration,
      "iconUri": iconUri,
      "mediaUri": mediaUri,
    };
  }

  @override
  String toString() {
    return 'MusicMetadata{mediaId: $mediaId, title: $title, subtitle: $subtitle}';
  }
}

class PlayQueue {
  final String queueId;

  /// nullable
  final String queueTitle;

  /// nullable
  final Map? extras;

  final List<MusicMetadata> queue;

  bool get isEmpty => queue.isEmpty;

  const PlayQueue(
      {required this.queueId,
      required this.queueTitle,
      this.extras,
      required this.queue});

  factory PlayQueue.fromMap(Map map) {
    return PlayQueue(
      queueId: map['queueId'],
      queueTitle: map['queueTitle'],
      extras: map['extras'],
      queue: (map['queue'] as List)
          .cast<Map>()
          .map((e) => MusicMetadata.fromMap(e))
          .toList(),
    );
  }

  Map toMap() {
    return {
      "queueId": queueId,
      "queueTitle": queueTitle,
      "extras": extras,
      "queue": queue.map((e) => e.toMap()).toList(),
    };
  }

  const PlayQueue.empty()
      : this(
            queueId: "empty",
            queueTitle: "empty",
            queue: const [],
            extras: const {});
}
