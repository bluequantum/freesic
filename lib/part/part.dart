library part;

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:overlay_support/overlay_support.dart';

export 'package:freesic/component/cache/cache.dart';
export 'package:freesic/component/player/player.dart';
export 'package:freesic/component/route.dart';
export 'package:freesic/material/dialogs.dart';
export 'package:freesic/material/dividers.dart';
export 'package:freesic/material/tiles.dart';
export 'package:freesic/model/model.dart';
export 'package:freesic/pages/account/account.dart';
export 'package:scoped_model/scoped_model.dart';
export 'package:freesic/component/global/orientation.dart';

// @deprecated
void notImplemented(BuildContext context) {
  toast('页面未完成');
}
